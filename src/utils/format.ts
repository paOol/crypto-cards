/* eslint-disable import/no-anonymous-default-export */
import currencyFormat from 'currency-formatter';

export const formatPercent=(value:number, precision:boolean = false) =>{
  let price = currencyFormat.format(value, {
    symbol: '%',
    decimal: '.',
    thousand: ',',
    precision: precision ? 2 : 0,
    format: '%v%s '
  });

  return price;
}

export const  formatCurrency = (value:number, symbol:string) =>{
  let price = currencyFormat.format(value, {
    symbol: symbol,
    decimal: '.',
    thousand: ',',
    precision: 2,
    format: '%s%v'
  });

  return price;
}

export default {
  formatCurrency,
  formatPercent,
};

