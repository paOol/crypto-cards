/* eslint-disable import/no-anonymous-default-export */
import axios from 'axios';

export const getMarketData = async (pageNumber:number): Promise<PriceData> => {
  const { data } = await axios.get(
    `https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=150&page=${pageNumber}&sparkline=true`
  );

  return data;
};

export interface PriceData {
  key: any;
  id: string;
  symbol: string;
  name: string;
  image: string;
  current_price: number;
  market_cap: number;
  market_cap_rank: number;
  fully_diluted_valuation: number;
  total_volume: number;
  high_24h: number;
  low_24h: number;
  price_change_24h: number;
  price_change_percentage_24h: number;
  market_cap_change_24h: number;
  market_cap_change_percentage_24h: number;
  circulating_supply: number;
  total_supply: number;
  max_supply: number;
  ath: number;
  ath_change_percentage: number;
  ath_date: Date;
  atl: number;
  atl_change_percentage: number;
  atl_date: Date;
  roi: Roi;
  last_updated: Date;
  sparkline_in_7d: Sparkline;
}

export interface Roi {
  times: number;
  currency: string;
  percentage: number;
}

export interface Sparkline {
  price: [number];
}

export default {
  getMarketData,
};
