import React from 'react';
import ContentLoader from 'react-content-loader'

export interface LoaderProps {
  height?: number;
  width: number;
}


export const Loader: React.FC<LoaderProps> = props => {


  const { width } = props

  return (
    <ContentLoader
      viewBox={`0 0 ${width} 54`}
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <circle cx="27" cy="27" r="18" />
      <rect x={0.16 * width} y="14" rx="3" ry="3" width={0.56 * width} height="13" />
      <rect x={0.16 * width} y="30" rx="3" ry="3" width={0.03 * width} height="10" />
      <rect x={0.21 * width} y="30" rx="3" ry="3" width={0.23 * width} height="10" />
      <rect x="0" y="53" rx="0" ry="0" width={width} height="1" />
      <rect x="219" y="146" rx="0" ry="0" width="0" height="0" />
    </ContentLoader>
  )
}


export default Loader;
