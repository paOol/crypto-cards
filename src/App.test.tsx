import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('render', () => {
  render(<App />);
  const linkElement = screen.getByText(/Paul Kim Engineer Assignment/i);
  expect(linkElement).toBeInTheDocument();
});
