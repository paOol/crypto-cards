/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components'
import { FixedSizeList as List } from "react-window";
import InfiniteLoader from "react-window-infinite-loader";

import { getMarketData, PriceData, } from './utils/api';
import { formatCurrency } from './utils/format';
import { Percentage } from './Percentage'
import { CanvasHook } from './CanvasHook'
import { Loader } from './Loading'

const LOADING: number = 1;
const LOADED: number = 2;
let itemStatusMap: any = {};

export const App = (): React.ReactElement => {

  const [isLoading, setIsLoading] = useState(true)
  const [priceData, setPriceData] = useState<PriceData[]>([]);
  const [containerWidth, setContainerWidth] = useState(100);
  const [pageNumber, setPageNumber] = useState(1);
  const Height = window.innerHeight;
  const wrapperRef = useRef<HTMLDivElement>(null);

  const isItemLoaded = (index: number): boolean => !!itemStatusMap[index];
  const loadMoreItems = async (startIndex: number, stopIndex: number): Promise<void> => {
    for (let index = startIndex; index <= stopIndex; index++) {
      itemStatusMap[index] = LOADING;
    }
    const ShouldLoadMore: boolean = (startIndex / priceData.length) >= 0.5
    if (ShouldLoadMore) {
      let newData = await getMarketData(pageNumber);
      let merged = priceData.concat(newData);
      setPageNumber(pageNumber => pageNumber + 1)
      setPriceData(merged);
    }

    for (let index = startIndex; index <= stopIndex; index++) {
      itemStatusMap[index] = LOADED;
    }
  };

  useEffect(() => {

    (async (): Promise<void> => {
      if (!priceData.length) {
        const data: any = await getMarketData(pageNumber);
        setPageNumber(pageNumber => pageNumber + 1)
        setPriceData(data);
        setIsLoading(false)
      }
    })();

    if (wrapperRef !== null) {
      const { current } = wrapperRef;
      if (current !== null) {
        setContainerWidth(current.clientWidth);
      }
    }

  }, [])


  if (isLoading) {
    return (
      <Container ref={wrapperRef}>
        <h5>Paul Kim Engineer Assignment</h5>

        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
        <Loader width={containerWidth} height={100} />
      </Container>)
  } else {

    return (
      <Container ref={wrapperRef}>

        <InfiniteLoader
          isItemLoaded={isItemLoaded}
          itemCount={5000}
          loadMoreItems={loadMoreItems}
        >
          {({ onItemsRendered, ref }) => {
            return (
              <List
                className="List"
                height={Height}
                width={containerWidth}
                itemCount={10000}
                itemSize={140}
                onItemsRendered={onItemsRendered}
                ref={ref}
                itemData={priceData}
              >
                {Row}
              </List>
            )
          }}
        </InfiniteLoader>
      </Container >
    );

  }
}


const Row = (props: { index: number, style: any, data: PriceData[] }): React.ReactElement => {

  const { index, style, data } = props;
  const [active, setActive] = useState(false)
  const [newStyle, setNewStyle] = useState(style)
  const handleClick = (): void => {
    let expanded = active ? 140 : 440
    setActive(!active)
    let newStyles = { ...newStyle }
    newStyles.height = expanded
    setNewStyle(newStyles)
  }



  if (index > data.length) {
    return <></>
  } else {
    if (itemStatusMap[index] === LOADED) {
      const { image, name, symbol, total_volume, current_price, price_change_percentage_24h, sparkline_in_7d } = data[index]
      return (
        <div style={newStyle}>
          <FlexContainer>
            <div className="coin" onClick={handleClick}>
              <div className="name">
                <img src={image} alt="" />
                <div>
                  {name}
                  <br />
                  <span>({symbol})</span>
                </div>
              </div>
              <div className="chart">
                < CanvasHook price={sparkline_in_7d.price} width={200} height={60} />
              </div>
              <div className="volume">
                24hr Volume
                  <br />
                {formatCurrency(total_volume, '$')}
              </div>
              <div className="price">
                {formatCurrency(current_price, '$')}
                <br />
                <Percentage value={price_change_percentage_24h} />
              </div>
            </div>
          </FlexContainer>
          <ExpandedContainer>
            <div className={active ? 'expanded active' : 'expanded'} >
              < CanvasHook price={sparkline_in_7d.price} width={1000} height={260} />
            </div>
          </ExpandedContainer>
        </div >
      );
    }
    else {
      return (
        <div className="ListItem" style={style}>
          <Loader width={1100} height={100} />
        </div>
      );
    }
  }
}

const Container = styled.div`
max-width:1100px;
margin: 0 auto;
.List{
  &::-webkit-scrollbar { width: 0 !important }
  -ms-overflow-style: none;
  overflow: -moz-scrollbars-none;
}
`
const ExpandedContainer = styled.div`
transition: 0.2s ease-out;

width:100%;
.expanded{
  visibility: hidden;
  height:0px;
}
.expanded.active{
  visibility: visible;
  width:100%;
  background: white;
  z-index: 1;
  position: absolute;
  box-shadow: 0 8px 20px rgba(0, 0, 0, 0.12);
  border-bottom: 1px solid rgb(221, 221, 221);
  height:300px;
}
`
const FlexContainer = styled.div`
  display:flex;
  flex-wrap: wrap;
  max-width: 100%;
  flex-basis: 100%;
  .coin{
    cursor:pointer;
    max-width: 100%;
    border: 1px solid rgb(221, 221, 221);
    border-radius: 6px;
    padding: 10px;
    margin-bottom:1rem;
    display: inline-flex;
    justify-content: space-between;
    flex-basis: 100%;
    align-items:center;
    img{
      max-width: 100px;
      max-height: 100px;
      margin-right:0.7rem;
    }
    .price{
      text-align: right;
      max-width:80px;
      flex-basis: 80px;
    }
    .name{
      max-width:240px;
      display: inline-flex;
      align-items: center;
      flex-basis: 240px;
      span{
        font-size: 0.7rem
      }
    }
    .chart{
            // max-width:140px;
            // flex-basis: 140px;
          }
    .volume{
      max-width:240px;
      flex-basis: 240px;
      text-align: right;
    }
    &:hover{
      box-shadow: 0 8px 20px rgba(0, 0, 0, 0.12);
      transition: 0.2s ease-out;
    }
  }
`

export default App;
