/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef } from 'react';

export interface CanvasHookProps {
  price: any;
  width: number;
  height: number;
}

interface Info {
  x: number,
  y: number,
  x1: number,
  y1: number
}
//HTMLCanvasElement
//CanvasRenderingContext2D
export const CanvasHook: React.FC<CanvasHookProps> = props => {

  const { price, width, height } = props;
  const canvasRef = useRef(null)

  const drawLine = (ctx: CanvasRenderingContext2D, info: Info) => {
    const { x, y, x1, y1 } = info;
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x1, y1);
    ctx.strokeStyle = 'deepskyblue';
    ctx.lineWidth = 2;
    ctx.stroke();
  }

  const formatPriceData = (price: any): Info[] => {

    const stepSize = width / price.length
    let currentStepX: number = 1;
    let currentStepY: number = 1;
    let array: Info[] = []

    const Ylargest = price.reduce((prev: number, current: number) => {
      return (current > prev) ? current : prev;
    })
    // const Ysmallest = price.reduce((prev: number, current: number) => {
    //   return (current < prev) ? current : prev;
    // })

    price.map((eachPrice: number) => {
      const x = currentStepX;
      const y = currentStepY;

      const getYpos = height - (eachPrice / Ylargest * height)

      currentStepX += stepSize;
      currentStepY = getYpos;
      let obj = {
        x,
        y,
        x1: currentStepX,
        y1: currentStepY
      }
      return array.push(obj)
    })
    return array
  }

  useEffect(() => {

    const canvas: any = canvasRef.current
    const ctx = canvas !== null && canvas.getContext('2d')

    ctx.width = width;
    ctx.height = height;

    let formatted = formatPriceData(price)
    for (const each of formatted) {
      drawLine(ctx, each)
    }

  }, [])

  return (
    <canvas ref={canvasRef} width={width} height={height} />
  )

}


export default CanvasHook;
