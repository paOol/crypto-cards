import React from 'react';
import { formatPercent } from './utils/format';

export interface PercentageProps {
  value: number;
}

export const Percentage: React.FC<PercentageProps> = props => {

  const { value } = props;
  if (value === null) {
    return null
  }
  if (value.toString().startsWith('-')) {
    return (<span className="percentage" style={{ "color": "#EB3B3B" }}>
      {formatPercent(value, true)}
    </span>)
  } else {
    return (<span className="percentage" style={{ "color": "#219653" }}>
      {formatPercent(value, true)}
    </span>)
  }
}


export default Percentage;
